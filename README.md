# Hacker News Feed app

## Reign Challenge

### Prerequisities

In order to run this container you'll need docker installed.

* Docker v19.03.13
* Docker-compose v1.27.4

## Usage

`` $ docker-compose up --build  ``

Then, you'll see the app running on 

``localhost:3000``


